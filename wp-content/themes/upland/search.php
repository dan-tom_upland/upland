  <section class="introduction">
  	<div class="container">
  		<div class="row">
  			<div class="col-lg-8 titleSection">
  				<h1>Wyszukiwarka</h1>
  				<h2><?php get_template_part('templates/page', 'header'); ?></h2>
  			</div>
  			<div class="col-lg-4">
  				<?php get_search_form(); ?>
  			</div>
  		</div>
  	</section>


  	<section class="blogNewest">
  		<div class="container">
  			<div class="row">
  				<div class="row newest">


  					<?php if (!have_posts()) : ?>
  						<div class="alert alert-warning">
  							<?php _e('Sorry, no results were found.', 'sage'); ?>
  						</div>
  					<?php endif; ?>

  					<?php while (have_posts()) : the_post(); ?>
  						<?php get_template_part('templates/content', 'post'); ?>
  					<?php endwhile; ?>



  				</div>
  			</div>     
  		</div>
  	</section>


  	<div class="container">
  		<div class="row">
  			<div class="col-sm-12">
  				<?php
  				if ( function_exists('wp_bootstrap_pagination') )
  					wp_bootstrap_pagination();
  				?>
  			</div>
  		</div>
  	</div>