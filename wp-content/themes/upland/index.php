  <section class="introduction">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-8 col-sm-7 titleSection">
  				<h1>Blog</h1>
  				<h2>Dzielimy się wiedzą i doświadczeniem</h2>
  			</div>
  			<div class="col-md-4 col-sm-5">
          <?php get_search_form(); ?>
        </div>
      </div>
    </section>


    <div class="container">
      <div class="row">
       <div class="col-sm-12">
        <?php if (!have_posts()) : ?>
         <div class="alert alert-warning">
          <?php _e('Sorry, no results were found.', 'sage'); ?>
        </div>  			
      <?php endif; ?>
    </div>
  </div>
</div>


<section class="blogNewest">
  <div class="container">
    <div class="row">
      <div class="row newest">
        <?php while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        <?php endwhile; ?>
      </div>
    </div>     
  </div>
</section>


<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <?php
      if ( function_exists('wp_bootstrap_pagination') )
        wp_bootstrap_pagination();
      ?>
    </div>
  </div>
</div>