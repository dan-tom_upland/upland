  <section class="introduction">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-8 col-sm-7 titleSection">
  				<h1>Publikacje autora</h1>
  				<h2>Autor: <?= get_the_author_meta('first_name'); ?> <?= get_the_author_meta('last_name'); ?></h2>
  			</div>
  			<div class="col-md-4 col-sm-5">
  				<?php get_search_form(); ?>
  			</div>
  		</div>
  	</section>


  	<section class="blogNewest">
  		<div class="container">
  			<div class="row">
  				<div class="row newest">


  					<?php if (!have_posts()) : ?>
  						<div class="alert alert-warning">
  							<?php _e('Sorry, no results were found.', 'sage'); ?>
  						</div>
  						<?php get_search_form(); ?>
  					<?php endif; ?>

  					<?php while (have_posts()) : the_post(); ?>
  						<?php get_template_part('templates/content', 'post'); ?>
  					<?php endwhile; ?>



  				</div>
  			</div>     
  		</div>
  	</section>


  	<div class="container">
  		<div class="row">
  			<div class="col-sm-12">
  				<?php
  				if ( function_exists('wp_bootstrap_pagination') )
  					wp_bootstrap_pagination();
  				?>
  			</div>
  		</div>
  	</div>