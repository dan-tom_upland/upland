  <section class="introduction">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-8 col-sm-7 titleSection">
  				<h1>Error 404</h1>
  				<h2><?php _e('Page you were trying to view does not exist.', 'sage'); ?></h2>
  			</div>
  			<div class="col-md-4 col-sm-5">
  				<?php get_search_form(); ?>
  			</div>
  		</div>
  	</section>
