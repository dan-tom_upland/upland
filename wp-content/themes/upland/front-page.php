



<section class="introCompany">

  <div class="container">
    <div class="row">
      <div class="col-md-7 col-sm-12 titleSection">



       <?php the_field('intro'); ?> 



     </div>
     <div class="col-md-5 hidden-sm hidden-xs"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/introCompanyAnim.png" class="img-responsive introCompanyAnim" /></div>
   </div>
 </div>

</section>




<?php if (get_locale() == 'pl_PL') { ?>

<section class="tag-list">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul>



          <li><?php the_field('check_out_our_last_article'); ?></li>


          <?php
          $args = array( 'numberposts' => '1' );
          $recent_posts = wp_get_recent_posts( $args );
          foreach( $recent_posts as $recent ){
            echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
          }
          wp_reset_query();
          ?>




        </ul>
        <a class="btn btn-default hidden-md hidden-sm hidden-xs" href="/blog/"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Blog</a>
      </div>
    </div>
  </div>
</section>


<?php } ?>





<section class="services">

 <div class="container">
  <div class="row">
    <div class="col-md-8 col-sm-7 titleSection">

      <?php the_field('usługi_tytuł'); ?> 


    </div>
    <div class="col-md-4 col-sm-5">

      <div class="personCard">
        <?php the_field('masz_pytanie'); ?> 
      </div>
    </div>
  </div>
</div>

<div class="line-service first">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
       <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/app.svg" class="img-responsive" alt="web development" />
     </div>
     <div class="col-sm-7 col-md-offset-1">

      <?php the_field('web_development'); ?> 

    </div>
  </div>
</div>
</div>       

<div class="line-service">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">

        <?php the_field('integracje'); ?> 



      </div>
      <div class="col-sm-5 col-md-offset-1">
        <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/integration-section.svg" class="img-responsive" alt="Integracje" />
      </div>
    </div>
  </div>
</div>

<div class="line-service">
  <div class="container">
    <div class="row">
      <div class="col-sm-5 col-md-offset-1">
        <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/mobile.svg" class="img-responsive" alt="e-commerce" />
      </div>
      <div class="col-sm-6">


       <?php the_field('mobile'); ?> 
     </div>

   </div>
 </div>
</div>

<div class="line-service">
  <div class="container">
    <div class="row">
     <div class="col-sm-6">

       <?php the_field('projektowanie'); ?> 
     </div>

     <div class="col-sm-5 col-md-offset-1">
       <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/design.svg" class="img-responsive" alt="design" />
     </div>
   </div>
 </div>
</div>
</section>



<section class="portfolioLine">

  <div class="container">
    <div class="row">
      <div class="col-sm-12 titleSection">
        <?php the_field('projekt_tytul'); ?> 
      </div>
    </div>
  </div>

  <?php the_field('projekt_1'); ?> 





</section>











<section class="reference">

  <div class="container">
    <div class="row">
      <div class="col-sm-12 titleSection">
        <?php the_field('referencje_tytul'); ?> 

      </div>
    </div>     
    <div class="row opinion">
      <?php the_field('referencje'); ?> 

    </div>
  </div>
</section>



<?php if (get_locale() == 'pl_PL') { ?>
<section class="blogNewestHome">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 titleSection">
        <?php the_field('blog_tytul'); ?> 
      </div>
    </div>     
    <div class="row newest">
      <?php $my_query = new WP_Query( 'posts_per_page=3&order=DESC' );
      while ( $my_query->have_posts() ) : $my_query->the_post();
        $do_not_duplicate = $post->ID; ?>
        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    </div>
  </div>
</section>
<?php } ?>


<section class="content-lay-1">

  <div class="container">
    <div class="row">
      <div class="col-sm-12 titleSection">
        <?php the_field('o_nas'); ?> 
      </div>
    </div>


    <div class="row tab">

      <?php the_field('o_nas_zawartosc'); ?> 


    </div>

  </div>

</section>



<section class="newsletterBlock">

  <div class="container">
    <div class="row">
     <div class="col-sm-2">
      <img src="/wp-content/themes/upland/dist/images/newsletter-icon.svg" class="img-responsive" alt="newsletter" />
    </div>





    <?php if (get_locale() == 'pl_PL') { ?>



    <div class="col-sm-4 titleSection">
      <h5>Zapisz się do Newslettera</h5>
      <h1>Aktualności z branży</h1>

    </div>
    <div class="col-sm-6">
     <?php echo do_shortcode('[contact-form-7 id="649" title="Newsletter pl"]' ); ?>
   </div>



   <?php } else { ?>



   <div class="col-sm-4 titleSection">
    <h5>Subscribe to the Newsletter</h5>
    <h1>Industry news</h1>

  </div>
  <div class="col-sm-6">
   <?php echo do_shortcode('[contact-form-7 id="672" title="Newsletter en"]' ); ?>
 </div>



 <?php } ?>






</div>
</div>


</section>

