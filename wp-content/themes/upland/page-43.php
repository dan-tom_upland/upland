

<section class="introduction">

 <?php the_field('intro'); ?> 
 <div id="particles-js"></div>
</section>

<section class="showCase">
  <div class="container">
    <div class="row">


     <?php the_field('tytul'); ?> 


   </div>
   <div class="row singleBox">



    <?php $parent = $post->ID;?>
    <?php $my_query = new WP_Query( 'posts_per_page=15&post_type=page&post_parent='.$parent . '&order=DESC' );
    while ( $my_query->have_posts() ) : $my_query->the_post();
      $do_not_duplicate = $post->ID; ?>




      <div class="col-sm-6">
       <div class="singleHalf single" style="background: url('<?php the_post_thumbnail_url(); ?>');">
        <a href="<?php the_permalink(); ?>">
          <div class="title">
            <h3><?php the_title(); ?></h3>
            <p><span><?php echo get_field('kategoria', $post->ID); ?></span>, <?php  echo get_field('firma', $post->ID); ?>, <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo get_field('miejsce', $post->ID); ?></p>
          </div>
        </a>
      </div>
    </div>




  <?php endwhile; ?>

  <?php wp_reset_postdata(); ?>


</div>
</section>


<section class="carrot">
  <div class="container">
    <div class="row">

     <?php the_field('zostan_klientem'); ?> 

   </div>
 </section>



 <script src="<?php bloginfo( 'template_url' ); ?>/dist/scripts/particles.min.js"></script>

 <script type="text/javascript">


  /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
  particlesJS.load('particles-js', '<?php bloginfo( 'template_url' ); ?>/assets/scripts/particles.json', function() {
    console.log('callback - particles.js config loaded');
  });

</script>