
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/logotyp.svg" alt="logotype" /></a>
      <div class="dropdown lang hidden-xs">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuLanguages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="fa fa-globe"></span>
          Language
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLanguages">
          <li><a href="/" hreflang="pl-PL" lang="pl-PL">Polish</a></li>
          <li><a href="/en/" hreflang="en-GB" lang="en-GB">English</a></li>
        </ul>
      </div>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <div class="navigation-right">


        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']);
        endif;
        ?>

        <div class="call2action">
          <?php if (get_locale() == 'pl_PL') { ?>
          <a href="/wycena-projektu/" class="btn btn-top">Wycena projektu</a>
          <?php } else { ?>
          <a href="/contact/" class="btn btn-top">Estimate project</a>
          <?php } ?>

        </div>
      </div>
    </div>
  </div>
</nav>