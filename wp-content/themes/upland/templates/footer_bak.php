 <?php  if (!is_page( array( 37, 264 ) )) { ?>


 








 <section class="socialIcon">
  <div class="container">
    <div class="row">
      <div class="col-sm-3 icon">
        <a href="https://clutch.co/profile/upland-digital"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/s-icon-1.png" alt="clutch"></a>
      </div>
      <div class="col-sm-3 icon">
        <a href="https://www.facebook.com/uplanddigital"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/s-icon-2.svg" alt="facebook"></a>
      </div>
      <div class="col-sm-3 icon">
        <a href="https://www.linkedin.com/company/upland-digital"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/s-icon-3.svg" alt="linkedin"></a>
      </div>
      <div class="col-sm-3 icon">
        <a href="https://www.instagram.com/upland.digital/"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/s-icon-4.svg" alt="instragram"></a>
      </div>
    </div>
  </div>
</section>


<section class="contact">
  <div class="content-right">
    <div class="container">
      <div class="row">
        <div class="col-xs-7 col-xs-offset-5 clickable">
          <div class="titleSection">
            <h1>Kontakt</h1>
            <h2>Zapraszamy do współpracy</h2>

          </div>

          <div class="row name">
            <div class="col-sm-12">
              <strong>Upland Digital sp. z o.o.</strong><br />
              <samp class="hidden-xs">Upland Digital sp. z o.o. z siedzibą we Wrocławiu, ul. Ruska 18/32, wpisana do rejestru przedsiębiorców Krajowego Rejestru Sądowego prowadzonego przez Sąd Rejonowy dla 
                Wrocławia-Fabrycznej we Wrocławiu,  VI Wydział Gospodarczy KRS pod numerem KRS 0000615459,
                NIP 8971823464, REGON 364322687.
              </samp>
            </div>
          </div>
          <div class="row contact-line">
            <div class="col-sm-5 blue">
              <a href="tel:&#053;&#055;&#057;&#054;&#052;&#048;&#048;&#055;&#048;"><i class="fa fa-phone" aria-hidden="true"></i> +&#052;&#056;&#032;&#053;&#055;&#057;&#032;&#054;&#052;&#048;&#032;&#048;&#055;&#048;</a><br />
              <a href="mailto:&#111;&#102;&#102;&#105;&#099;&#101;&#064;&#117;&#112;&#108;&#097;&#110;&#100;&#046;&#100;&#105;&#103;&#105;&#116;&#097;&#108;"><i class="fa fa-envelope-o" aria-hidden="true"></i> &#111;&#102;&#102;&#105;&#099;&#101;&#064;&#117;&#112;&#108;&#097;&#110;&#100;&#046;&#100;&#105;&#103;&#105;&#116;&#097;&#108;</a>
            </div>
            <div class="col-sm-4">
              Ruska 18/32<br />
              50-079 Wrocław
            </div>
            <div class="col-sm-3">
             <i class="fa fa-calendar" aria-hidden="true"></i> Pn – Pt<br />
             <i class="fa fa-clock-o" aria-hidden="true"></i> 09:00 – 17:00
           </div>
         </div>

         <div class="row hidden-xs">
          <?php echo do_shortcode( '[contact-form-7 id="262" title="Formularz kontaktowy"]' ); ?>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="contact-bg"></div>
<div id="map"></div>
</section>



<?php } ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAemM6tKrJ7mi8lHazWiykyrWnIAvPu81s&callback=initMap"
async defer></script>



<script type="text/javascript">

  $('.shot').click(function() {
    $(this).toggleClass('shotFull');
    $(this).closest('div').siblings('.contenShot').toggleClass('fadeContent');
  });

  $('.about').find('.shotButton').click(function() {
    console.log(this);
    $(this).parent().parent().parent().find('.shot').toggleClass('shotFull');
    $(this).parent().parent().parent().find('.shot').closest('div').siblings('.contenShot').toggleClass('fadeContent');
  });




  function initMap() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 14,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(51.1109399, 17.0616105),

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
                  };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                  position: new google.maps.LatLng(51.110500, 17.0261949),
                  map: map,
                  icon: '<?php bloginfo( 'template_url' ); ?>/dist/images/marker.png'
                });
              }




            </script>


            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAemM6tKrJ7mi8lHazWiykyrWnIAvPu81s&callback=initMap"
            async defer></script>