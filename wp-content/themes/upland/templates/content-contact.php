







<section class="contact">



  <div class="content-right">
    <div class="container">
      <div class="row">
        <div class="col-xs-7 col-xs-offset-5 clickable">

          <?php if (get_locale() == 'pl_PL') { ?>
          <?php the_field('kontakt', 34); ?> 
          <div class="row hidden-xs hidden-sm">
            <?php echo do_shortcode('[contact-form-7 id="262" title="Formularz kontaktowy"]' ); ?>
            <?php } else { ?>
            <?php the_field('kontakt', 689); ?> 
            <div class="row hidden-xs hidden-sm">
              <?php echo do_shortcode('[contact-form-7 id="698" title="Formularz kontaktowy en"]' ); ?>
              <?php } ?>

            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="contact-bg"></div>
    <div id="map"></div>


  </section>




  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAemM6tKrJ7mi8lHazWiykyrWnIAvPu81s&callback=initMap"
  async defer></script>



  <script type="text/javascript">







    function initMap() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)


                    <?php if (get_locale() == 'pl_PL') { ?>
                      zoom: 14,
                      center: new google.maps.LatLng(51.1109399, 17.0616105),
                      <?php } else { ?>
                        zoom: 5,
                        center: new google.maps.LatLng(51.1109399, 30.0116105),
                        <?php } ?>

                    // The latitude and longitude to center the map (always required)


                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
                  };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                  position: new google.maps.LatLng(51.110500, 17.0261949),
                  map: map,
                  icon: '<?php bloginfo( 'template_url' ); ?>/dist/images/marker.png'
                });
              }




            </script>


            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAemM6tKrJ7mi8lHazWiykyrWnIAvPu81s&callback=initMap"
            async defer></script>