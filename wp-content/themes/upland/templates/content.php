



<div <?php post_class(); ?>>
	<div class="postInside" style="background: url('<?php the_post_thumbnail_url(); ?>');">
		<a href="<?php the_permalink(); ?>">
			<div class="title">
				<i class="fa fa-long-arrow-right" aria-hidden="true"></i> <?php the_title(); ?>
				<div class="exc"><?php the_excerpt(); ?></div>
				<div class="about">
					<div class="author-img"><?php echo get_avatar( get_the_author_meta( 'ID' ), 32 ); ?></div>
					<div class="author">
						<div class="name" rel="author"><?= get_the_author_meta('first_name'); ?> <?= get_the_author_meta('last_name'); ?></div>
						<time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
					</div>
					
					<div class="category">
						

						<?php


						$categories = get_the_category();
						$separator = ' &#xb7; ';
						$output = '';
						if ( ! empty( $categories ) ) {
							foreach( $categories as $category ) {
								$output .= esc_html( $category->name ) . $separator;
							}
							echo trim( $output, $separator );
						}


						?>

					</div>

				</div>
			</div>
		</a>
	</div>
</div>
