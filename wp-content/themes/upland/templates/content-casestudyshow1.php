<div class="projectPresent">

  <section class="introductionProject" style="background: url(<?php echo get_field('zdjęcie_w_tle'); ?>)">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="navProject">

           <?php previous_link(); ?>

           <a href="/"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/icon-project.svg" class="projectShow" alt="project show"></a>


           <?php next_link(); ?>

         </div>

         <div class="titleProject">
          <h1><?php the_title(); ?></h1>
        </div>

        <?php if (get_field('podgląd_projektu_dla_zdjęcia_w_tle')) { ?>
        <img src="<?php echo get_field('podgląd_projektu_dla_zdjęcia_w_tle'); ?>" class="img-responsive device" alt="device" />
        <?php } ?>
      </div>
    </div>
  </div>
</section>





<section class="about">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">

        <?php the_field('text_1'); ?> 
        <?php echo $post->post_content; ?>
      </div>
    </div>
  </div>
</section>

<section class="aboutNumber">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="iconBox">
         <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/portfolio-icon-1.svg" alt="icon">
       </div>
       <h4><?php echo get_field('firma'); ?></h4>
     </div>
     <div class="col-sm-3">
      <div class="iconBox">
       <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/portfolio-icon-2.svg" alt="icon">
     </div>
     <h4><?php echo get_field('kategoria'); ?></h4>
   </div>
   <div class="col-sm-3">
    <div class="iconBox">
      <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/portfolio-icon-3.svg" alt="icon">
    </div>
    <h4><?php echo get_field('miejsce'); ?></h4>
  </div>
  <div class="col-sm-3">
    <div class="iconBox">
     <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/portfolio-icon-4.svg" alt="icon">
   </div>
   <h4><?php echo get_field('czas_realizacji'); ?></h4>
 </div>
</div>
</div>
</section>



<section class="opinion">
  <div class="container">
    <div class="row">
     <?php if (get_field('czy_klient_dal_referencje')) { ?>
     <div class="col-sm-6">
      <div class="titleSection">
        <?php the_field('text_2'); ?> 

      </div>
      <div class="panel panel-default">
        <div class="panel-body">
          <?php echo get_field('opinia_klienta'); ?>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="<?php if (get_field('czy_klient_dal_referencje')) { ?>col-sm-6<?php } else { ?>col-sm-6 col-sm-offset-3 technology-center<?php } ?>">
      <div class="titleSection">
        <?php the_field('text_3'); ?> 

      </div>
      <div class="iconSpec">
        <div class="icon">



          <?php
          $values = get_field('technologie');
          $num = 0;
          if($values)
          {
            foreach($values as $value)
            {

              $num ++;

              ?>
              <?php if ($num < 5) {?>
              <div class="col-xs-3">
                <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/t-icon-<?php echo $value ?>.svg" class="img-responsive" alt="icon" data-toggle="tooltip" data-placement="bottom" title="<?php echo $value ?>"/>
              </div> 
              <?php
              
            } elseif ($num == 5) {
              ?>
            </div>
            <div class="icon">
              <div class="col-xs-3 col-sm-offset-1">
                <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/t-icon-<?php echo $value ?>.svg" class="img-responsive" alt="icon" data-toggle="tooltip" data-placement="bottom" title="<?php echo $value ?>"/>
              </div>

              <?php
            } elseif ($num > 5) { ?>

            <div class="col-xs-3">
              <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/t-icon-<?php echo $value ?>.svg" class="img-responsive" alt="icon" data-toggle="tooltip" data-placement="bottom" title="<?php echo $value ?>"/>
            </div> 

            <?php } 




          }  
        }
        ?>
      </div>


    </div>
  </div>
</div>
</div>
</section>


<section class="shotPreview">
  <div class="container">
    <div class="row title">
      <div class="col-sm-12">
        <?php the_field('text_4'); ?> 
        
      </div>
    </div>


    <?php if (get_field('podglad_1_–_opis')) { ?>
    <div class="row about">
      <div class="col-sm-4 contenShot">
        <?php echo get_field('podglad_1_–_opis'); ?>

        <p class="infoPrev"><button class="button btn-default shotButton"><i class="fa fa-arrows-alt" aria-hidden="true"></i> <?php the_field('text_5'); ?></button></p>
      </div>
      <div class="col-sm-8">
        <img src="<?php echo get_field('podglad_1_–_zdjecie'); ?>" class="shot" alt="podgląd projektu" />
      </div>
    </div>
    <?php } ?>

    <?php if (get_field('podglad_2_–_opis')) { ?>
    <div class="row about aboutOdd">
      <div class="col-sm-8">
        <img src="<?php echo get_field('podglad_2_–_zdjecie'); ?>" class="shot" alt="podgląd projektu" />
      </div>
      <div class="col-sm-4 contenShot">
        <?php echo get_field('podglad_2_–_opis'); ?>
        <p class="infoPrev"><button class="button btn-default shotButton"><i class="fa fa-arrows-alt" aria-hidden="true"></i> <?php the_field('text_5'); ?></button></p>
      </div>
    </div>
    <?php } ?>

    <?php if (get_field('podglad_3_–_opis')) { ?>
    <div class="row about">
      <div class="col-sm-4 contenShot">
        <?php echo get_field('podglad_3_–_opis'); ?>
        <p class="infoPrev"><button class="button btn-default shotButton"><i class="fa fa-arrows-alt" aria-hidden="true"></i> <?php the_field('text_5'); ?></button></p>
      </div>
      <div class="col-sm-8">
        <img src="<?php echo get_field('podglad_3_–_zdjecie'); ?>" class="shot" alt="podgląd projektu" />
      </div>
    </div>
    <?php } ?>


    <?php if (get_field('podglad_4_–_opis')) { ?>
    <div class="row about aboutOdd">
      <div class="col-sm-8">
        <img src="<?php echo get_field('podglad_4_–_zdjecie'); ?>" class="shot" alt="podgląd projektu" />
      </div>
      <div class="col-sm-4 contenShot">
        <?php echo get_field('podglad_4_–_opis'); ?>
        <p class="infoPrev"><button class="button btn-default shotButton"><i class="fa fa-arrows-alt" aria-hidden="true"></i> <?php the_field('text_5'); ?></button></p>
      </div>
    </div>
    <?php } ?>

    <?php if (get_field('podglad_5_–_opis')) { ?>
    <div class="row about">
      <div class="col-sm-4 contenShot">
        <?php echo get_field('podglad_5_–_opis'); ?>
        <p class="infoPrev"><button class="button btn-default shotButton"><i class="fa fa-arrows-alt" aria-hidden="true"></i> <?php the_field('text_5'); ?></button></p>
      </div>
      <div class="col-sm-8">
        <img src="<?php echo get_field('podglad_5_–_zdjecie'); ?>" class="shot" alt="podgląd projektu" />
      </div>
    </div>
    <?php } ?>

    <?php if (get_field('podglad_6_–_opis')) { ?>
    <div class="row about aboutOdd">
      <div class="col-sm-8">
        <img src="<?php echo get_field('podglad_6_–_zdjecie'); ?>" class="shot" alt="podgląd projektu" />
      </div>
      <div class="col-sm-4 contenShot">
        <?php echo get_field('podglad_6_–_opis'); ?>
        <p class="infoPrev"><button class="button btn-default shotButton"><i class="fa fa-arrows-alt" aria-hidden="true"></i> <?php the_field('text_5'); ?></button></p>
      </div>
    </div>
    <?php } ?>







  </div>
</section>

<section class="contactFormBottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-sm-offset-4">

        <?php echo do_shortcode( '[contact-form-7 id="261" title="Formularz strona projektu"]' ); ?>

        

      </div>
    </div>
  </div>
</section>

<section class="showCase showcaseBottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <div class="titleSection">
          <?php the_field('text_6'); ?> 
          
        </div>
      </div>
    </div>
    <div class="row singleBox">
      <?php $parent = 43;?>
      <?php
      query_posts('posts_per_page=2&post_type=page&post_parent='.$parent . '&order=ASC' . '&orderby=rand');
      while (have_posts()) : the_post();
        ?>
        <div class="col-sm-6">
          <div class="singleHalf single gpc" style="background: url('<?php the_post_thumbnail_url(); ?>');">
            <a href="<?php the_permalink(); ?>">
              <div class="title">
                <h3><?php the_title(); ?></h3>
                <p><span><?php echo get_field('kategoria', $post->ID); ?></span>, <?php  echo get_field('firma', $post->ID); ?>, <?php echo get_field('miejsce', $post->ID); ?></p>
              </div>
            </a>
          </div>
        </div>
      <?php endwhile;?>
    </div>
  </section>

</div>




