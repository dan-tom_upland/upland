<section class="services service-page">
  <div class="container">

    <div class="row">
      <div class="col-sm-12">
        <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><img src="/wp-content/themes/upland/dist/images/logotyp.svg" alt="logotype"></a>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-7 line-service">



        <?php the_field('intro_uslug'); ?> 
      </div>
      <div class="col-sm-4 col-sm-offset-1">

        <span class="contactinfo">
          <a href="tel:579640070"><i class="fa fa-phone" aria-hidden="true"></i> +48 579 640 070</a>
        </span>

        <section class="contactFormBottom">
         <?php if (get_locale() == 'pl_PL') { ?>
         <?php echo do_shortcode( '[contact-form-7 id="261" title="Formularz strona projektu"]' ); ?>
         <?php } else { ?>

         <?php echo do_shortcode( '[contact-form-7 id="673" title="Formularz strona en"]' ); ?>
         <?php } ?>
         
       </section>

     </div>
   </div>
 </div>
</section>



<section class="boxDescription">
  <div class="container">

    <div class="row">









      <?php the_field('box_uslugi_1'); ?> 
      <?php the_field('box_uslugi_2'); ?> 
      <?php the_field('box_uslugi_3'); ?> 
      <?php the_field('box_uslugi_4'); ?> 
      <?php the_field('box_uslugi_5'); ?> 
      <?php the_field('box_uslugi_6'); ?> 


    </div>
  </div>
</section>