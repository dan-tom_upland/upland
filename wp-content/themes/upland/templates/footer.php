

<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/logotyp_white.svg" class="img-responsive" />


        <span class="contactinfo">
          <a href="tel:579640070"><i class="fa fa-phone" aria-hidden="true"></i> +48 579 640 070</a>
          <br />
          <a href="mailto:office@upland.digital"><i class="fa fa-envelope-o" aria-hidden="true"></i> office@upland.digital</a>
          <br /><br />
          <span class="small-contact">
            <strong>VAT-ID (NIP):</strong> PL8971823464
            <br />
            <strong>REGON:</strong> 364322687
            <br />
            <strong>KRS:</strong> 0000615459
          </span>
        </span>
      </div>

      <?php if (get_locale() == 'pl_PL') { ?>
      <div class="col-sm-2">
        <h4>O firmie</h4>
        <ul>
          <li><a href="/o-nas/">O nas</a></li>
          <li><a href="/uslugi/">Usługi</a></li>
          <li><a href="/klienci/">Klienci</a></li>
          <li><a href="/blog/">Blog</a></li>
          <li><a href="/Kariera/">Kariera</a></li>
          <li><a href="/kontakt/">Kontakt</a></li>
        </ul>


        <?php } else { ?>
        <div class="col-sm-4">

          <h4>About company</h4>
          <ul>
            <li><a href="/about-us/">About us</a></li>
            <li><a href="/services/">Services</a></li>
            <li><a href="/client/">Client</a></li>
            <li><a href="/contact/">Contact</a></li>
          </ul>


          <?php } ?>








        </div>
        <?php if (get_locale() == 'pl_PL') { ?>
        <div class="col-sm-2">
         <h4>Kategorie na blogu</h4>
         <ul>
          <li><a href="/blog/category/start-w-ecommerce-pl/">Start w eCommerce</a></li>
          <li><a href="/blog/category/omnichannel-pl/">Omnichannel</a></li>
          <li><a href="/blog/category/development-pl/">Development</a></li>
          <li><a href="/blog/category/design/">Design</a></li>
        </ul>
      </div>

      <?php } ?>

      <div class="col-sm-4 col-sm-offset-1 maps">
       <h4><?php if (get_locale() == 'pl_PL') { ?>Nasze biura <?php } else { ?>Our offices<?php } ?></h4>
       <span class="place">
         <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/pin.svg" class="img-responsive" />
         <p>
          <strong>Poland (HQ)</strong><br />
          Ruska 18/32<br> 50-079 Wrocław
        </p>
      </span>

      <span class="place">
       <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/pin.svg" class="img-responsive" />
       <p>
        <strong>Poland</strong><br />
        Piłsudskiego 74/306<br> 50-020 Wrocław
      </p>
    </span>

    <span class="place">
     <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/pin.svg" class="img-responsive" />
     <p><strong>Niemcy</strong><br />
       Siemens Straße 22<br> 59199 Bönen</p>
     </span>

   </div>
 </div>

 <div class="row">
  <hr />
  <div class="col-sm-6 copy">
    2017 © Copyright Upland.Digital
  </div>
  <div class="col-sm-6 hidden-xs">
    <div class="icon">
      <a href="https://clutch.co/profile/upland-digital"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/s-icon-1.png" alt="clutch"></a>
      <a href="https://www.facebook.com/uplanddigital"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/s-icon-2.svg" alt="facebook"></a>
      <a href="https://www.linkedin.com/company/upland-digital"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/s-icon-3.svg" alt="linkedin"></a>
      <a href="https://www.instagram.com/upland.digital/"><img src="<?php bloginfo( 'template_url' ); ?>/dist/images/s-icon-4.svg" alt="instragram"></a>
    </div>
  </div>
</div>

</div>
</footer>







<!-- END CONTENT / SCRIPT FOOTER -->

<script src="<?php bloginfo( 'template_url' ); ?>/assets/scripts/jquery-2.2.4.min.js" | prepend: site.baseurl }}" type="text/javascript"></script>

<script src="<?php bloginfo( 'template_url' ); ?>/assets/scripts/particles.min.js" | prepend: site.baseurl }}" type="text/javascript"></script>


<script src="https://use.fontawesome.com/0e0bb574a8.js" type="text/javascript" async></script>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102760244-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
  (function (w,i,d,g,e,t,s) {w[d] = w[d]||[];t= i.createElement(g);
    t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t, s);
  })(window, document, '_gscq','script','//widgets.getsitecontrol.com/76943/script.js');
</script>

<script src='https://cdn.slaask.com/chat.js'></script>
<script>
 if ($(window).width() > 960) {
  _slaask.init('aa13e67b419d94fd51141e01335a2724');
}
</script>


<!-- Google Code for Konwersja www Conversion Page -->
<script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 844446764;
  var google_conversion_language = "en";
  var google_conversion_format = "3";
  var google_conversion_color = "ffffff";
  var google_conversion_label = "e1xGCLr8qXMQrPjUkgM";
  var google_remarketing_only = false;
  /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
  <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/844446764/?label=e1xGCLr8qXMQrPjUkgM&amp;guid=ON&amp;script=0"/>
  </div>
</noscript>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3CNWL3"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->


  <script>
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      ga('send', 'event', 'Contact Form', 'submit');
    }, false );
  </script>


  <!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/2975697.js"></script>
  <!-- End of HubSpot Embed Code -->



  <script type="text/javascript">

    $('.shot').click(function() {
      $(this).toggleClass('shotFull');
      $(this).closest('div').siblings('.contenShot').toggleClass('fadeContent');
    });

    $('.about').find('.shotButton').click(function() {
      console.log(this);
      $(this).parent().parent().parent().find('.shot').toggleClass('shotFull');
      $(this).parent().parent().parent().find('.shot').closest('div').siblings('.contenShot').toggleClass('fadeContent');
    });

  </script>