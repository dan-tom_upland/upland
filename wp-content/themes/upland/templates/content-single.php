<?php while (have_posts()) : the_post(); ?>
  <div class="newsPresent">
    <section class="introductionNews" style="background: url(<?php the_post_thumbnail_url(); ?>)">
      <div class="darkbg">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
              <div class="titleProject">
               <div class="about">
                 <a href="<?= bloginfo( 'url' ); ?>/blog/author/<?= the_author_link(); ?>" class="author-img"><?php echo get_avatar( get_the_author_meta( 'ID' ), 64 ); ?></a>
                 <div class="author">
                  <a href="<?= bloginfo( 'url' ); ?>/blog/author/<?= the_author_link(); ?>" rel="author" class="name"><?= get_the_author_meta('first_name'); ?> <?= get_the_author_meta('last_name'); ?></a>

                </div>
                <h1><?php the_title(); ?></h1>
                <div class="row bottom">
                  <div class="col-sm-12">
                    <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
                    <div class="category"><?php echo get_the_category_list(); ?></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2">
      <article <?php post_class(); ?>>

        <div class="entry-content">
          <?php the_content(); ?>
        </div>

      </article>

    </div>
  </div>
</div>

<?php endwhile; ?>

<div class="comments">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <footer>
          <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
        </footer>
        <?php comments_template('/templates/comments.php'); ?>
      </div>
    </div>
  </div>
</div>