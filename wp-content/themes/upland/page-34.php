<div class="o-nas">











  <section class="processService">


   <?php the_field('proces'); ?> 

 </section>








 <section class="introduction">

   <?php the_field('o_nas'); ?> 

 </section>




 <section class="features">

   <?php the_field('wyroznik'); ?> 
 </section>




<!--  <section class="moreAbout titleSection">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h3>O firmie</h3>
          <p>Siedziba Upland Digital sp. z o.o. mieści się w ścisłym centrum Wrocławia przy ulicy Ruskiej 18.
            <br /><br />
            Biuro na wrocławskim rynku zapewnia komfortowe warunki do spotkań biznesowych oraz pracy nad ambitnymi projektami. Mamy doskonałą lokalizację. Znajdujemy się na przecięciu najważniejszych szlaków komunikacyjnych.
          </p>
        </div>
        <div class="col-sm-6">
          <h3>Zespół</h3>
          <p>Jesteśmy zespołem składającym się z projektanów i programistów. Naszym celem nie tylko jest dostarczanie dobrze wyglądających aplikacji. Uwielbiamy świetnie zaprojektowane aplikacje ale były by bezużyteczne gdyby nie nasi programiści którzy sprawiają, że przyjemne doznania wynikają z interakcji. 

            Jesteśmy na bieżąco z najnowszymi technologiami, przez co dużo sprawniej realizujemy powierzone nam projekty. Korzystamy ze zwinnych metodyk tworzenia oprogramowania, które pozwalają nam skupić się na tworzeniu nowych funkcjonalności w tygodniowych cyklach tak, abyś regularnie widział efekty naszej pracy.
          </p>
        </div>
      </div>
    </div>
  </section> -->


  <section class="reference">


   <?php the_field('referencje'); ?> 
 </section>




 <section class="ourTechnology">

   <?php the_field('technologie'); ?> 


 </section>






 <section class="contact">



  <div class="content-right">
    <div class="container">
      <div class="row">
        <div class="col-xs-7 col-xs-offset-5 clickable">



          <?php the_field('kontakt', 34); ?> 


          <div class="row hidden-xs hidden-sm">
            <?php echo do_shortcode('[contact-form-7 id="262" title="Formularz kontaktowy"]' ); ?>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="contact-bg"></div>
  <div id="map"></div>


</section>




<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAemM6tKrJ7mi8lHazWiykyrWnIAvPu81s&callback=initMap"
async defer></script>



<script type="text/javascript">







  function initMap() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 14,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(51.1109399, 17.0616105),

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
                  };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                  position: new google.maps.LatLng(51.110500, 17.0261949),
                  map: map,
                  icon: '<?php bloginfo( 'template_url' ); ?>/dist/images/marker.png'
                });
              }




            </script>


            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAemM6tKrJ7mi8lHazWiykyrWnIAvPu81s&callback=initMap"
            async defer></script>



          </div>