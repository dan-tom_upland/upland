<div class="uslugi">











  <section class="services">

   <div class="container">
    <div class="row">
      <div class="col-md-12 titleSection titleIntro">


        <?php the_field('usługi_tytuł', 511); ?> 

      </div>
      
    </div>
  </div>

  <div class="line-service first">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
         <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/app.svg" class="img-responsive" alt="web development" />
       </div>
       <div class="col-sm-7 col-md-offset-1">

        <?php the_field('web_development', 511); ?> 

      </div>
    </div>
  </div>
</div>       

<div class="line-service">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">

        <?php the_field('integracje', 511); ?> 



      </div>
      <div class="col-sm-5 col-md-offset-1">
        <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/integration-section.svg" class="img-responsive" alt="Integracje" />
      </div>
    </div>
  </div>
</div>

<div class="line-service">
  <div class="container">
    <div class="row">
      <div class="col-sm-5 col-md-offset-1">
        <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/mobile.svg" class="img-responsive" alt="e-commerce" />
      </div>
      <div class="col-sm-6">


       <?php the_field('mobile', 511); ?> 
     </div>

   </div>
 </div>
</div>

<div class="line-service">
  <div class="container">
    <div class="row">
     <div class="col-sm-6">

       <?php the_field('projektowanie', 511); ?> 
     </div>

     <div class="col-sm-5 col-md-offset-1">
       <img src="<?php bloginfo( 'template_url' ); ?>/dist/images/design.svg" class="img-responsive" alt="design" />
     </div>
   </div>
 </div>
</div>
</section>


<section class="contactFormBottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-sm-offset-4">

        <?php echo do_shortcode( '[contact-form-7 id="261" title="Formularz strona projektu"]' ); ?>

        

      </div>
    </div>
  </div>
</section>




</div>