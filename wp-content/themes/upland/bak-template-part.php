// Co nas wyróżnia

<section class="whyus">

	<div class="container">
		<div class="row">
			<div class="col-sm-12 titleSection">
				<h1>Co nas wyróżnia?</h1>
				<h2>Sprawdź, dlaczego warto nawiązać współpracę</h2>
			</div>
		</div>
		<div class="row slider">
			<div class="col-md-8 col-md-offset-2">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="carousel-caption">
								<h4>Indywidualne podejście do Klienta</h4>
								<p>W odróżnieniu od wielu firm, nasz zespół nie skupia się na ilości realizowanych projektów. Ważna jest dla nas jakość i końcowy efekt realizacji.</p>
							</div>
						</div>
						<div class="item">

							<div class="carousel-caption">
								<h4>Bardzo dobre zaplecze warsztatowe</h4>
								<p>Każda nasza realizacja jest wyjątkowa i niepowtarzalna. Nie powielamy schematów. Nasze rozwiązania stanowią przykład dobrego designu.</p>
							</div>
						</div>
						<div class="item">
							<div class="carousel-caption">
								<h4>Pełna kontrola nad realizacją projektu</h4>
								<p>Realizacja powierzonych nam projektów oraz procesy wewnątrz firmy są transparentne. Masz pełną kontrolę nad każdym etapem projektu. Będąc naszym Klientem, stajesz się członkiem naszego zespołu. Zapewniamy dostęp do platformy komunikacyjnej oraz systemu zarządzania zadaniami.</p>
							</div>
						</div>
					</div>

					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<i class="fa fa-angle-left" aria-hidden="true"></i>
						<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<i class="fa fa-angle-right" aria-hidden="true"></i>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>